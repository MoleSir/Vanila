class C:
    a = 1
    def __init__(self) -> None:
        pass

c1 = C()
c1.a = 2
print(c1.a)
C.a = 12
c2 = C()

print(C.a)
print(c1.a)
print(c2.a)

C.b =1

print(C.b)