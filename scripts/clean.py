from constant import *
import os

for obj_file in os.listdir(OBJ_FOLDER):
    os.remove(OBJ_FOLDER + obj_file)

for exe_file in os.listdir(EXE_FOLDER):
    if exe_file.endswith('.exe'):
        os.remove(EXE_FOLDER + exe_file)