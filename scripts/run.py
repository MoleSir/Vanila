from constant import *
import subprocess
import sys

try:
    if len(sys.argv) < 3:
        subprocess.check_call([exe_path])
    else:
        subprocess.check_call([exe_path, sys.argv[2]])
except subprocess.CalledProcessError:
    print('\033[1;31;40mexecute application failed\033[0m')