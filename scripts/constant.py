import sys

SRC_FOLDER = './src/' 
OBJ_FOLDER = './bin/obj/'
EXE_FOLDER = './bin/exe/'
INC_FOLDER = './include/'
LIB_FOLDER = './lib/'

DEBUG_EXE_PATH = './bin/exe/vanila_debug.exe'
RELEASE_EXE_PATH = './bin/exe/vanila.exe'

# 输出可执行文件路径
exe_path = DEBUG_EXE_PATH if sys.argv[1] == 'debug' else RELEASE_EXE_PATH