#ifndef __UTILS_CHAR_CHECK_HH__
#define __UTILS_CHAR_CHECK_HH__

namespace utils
{

inline bool isDigit(char c) noexcept
{
    return c >= '0' && c <= '9';
}

inline bool isAlpha(char c) noexcept
{
    return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || c == '_';
}



}

#endif