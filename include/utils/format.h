#ifndef __VANLIA_FORMAT_HH__
#define __VANLIA_FORMAT_HH__

#include <string>

namespace utils
{
//! \brief format the string to a std::string
std::string format(const char* fmt, ...);
}

#endif