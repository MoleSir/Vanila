#ifndef __UTILS_HASH_HH__
#define __UTILS_HASH_HH__

#include <cstdint>

namespace utils
{
uint32_t fnv1aHash(const char* key, uint32_t length) noexcept;
}

#endif