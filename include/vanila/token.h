#ifndef __VANILA_TOKEN_HH__
#define __VANILA_TOKEN_HH__

#include <cstdint>

namespace vanila
{
enum class TokenType
{
    // Single-character tokens. 
    LEFT_PAREN, RIGHT_PAREN,
    LEFT_BRACE, RIGHT_BRACE,
    LEFT_BRACKET, RIGHT_BRACKET, 
    COMMA, COLON, DOT, MINUS, PLUS,
    SEMICOLON, SLASH, STAR,

    // One or two character tokens. 
    BANG, BANG_EQUAL,
    EQUAL, EQUAL_EQUAL,
    GREATER, GREATER_EQUAL,
    LESS, LESS_EQUAL,
    
    // Literals. 
    IDENTIFIER, STRING, INTEGER, DECIMAL,
    
    // Keywords.
    AND, CLASS, ELSE, FALSE,
    FOR, FUN, IF, NIL, OR,
    RETURN, SUPER, THIS,
    TRUE, VAR, WHILE,

    ERROR, ENDOFFILE
};

struct Token
{
    TokenType type;
    const char* start;
    uint32_t length;
    uint32_t line;

    Token() noexcept :
        type{TokenType::ERROR}, start{nullptr}, length{0}, line{0} {}

    Token(TokenType t, const char* s, uint32_t len, uint32_t l) noexcept :
        type{t}, start{s}, length{len}, line{l} {}

    Token(const char* text) noexcept;

    //! \brief check wheather two tokens name is equal
    static bool identifiersEqual(const Token& a, const Token& b) noexcept; 
};

}

#endif