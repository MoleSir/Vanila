#ifndef __VANILA_EXCEPTION_HH__
#define __VANILA_EXCEPTION_HH__

#include <string>

namespace vanila
{

class Error
{
public:
    Error() noexcept {};
    ~Error() noexcept {};

public:
    virtual std::string what() const = 0;
};

class RuntimeError : public Error
{
public:
    RuntimeError() noexcept {};
    ~RuntimeError() noexcept {};

private:

    virtual std::string what() const override;
};

class CompileError : public Error
{
public:
    CompileError() noexcept {};
    ~CompileError() noexcept {};

private:
    virtual std::string what() const override;
};


}

#endif