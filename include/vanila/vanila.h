#ifndef __VANILA_HH__
#define __VANILA_HH__

#include "vanila/virtualmachine.h"
#include "vanila/error.h"
#include <string>

namespace vanila
{
                                              
class Vanila
{
public:
    //! \brief release assert
    static void release() noexcept;

    //! \brief init assert
    static void init();

    //! \brief interactive interface
    static void interactive();

    //! \brief run a script file
    //! \param[in] path script file path
    static void runFile(std::string path);

private:
    static VirtualMachine* vm;
    static const char* label;

    //! \brief read source code from script file
    static std::string readFile(const std::string& path);

    //! \brief interpret the source code
    static void interpret(const std::string& source) noexcept;
};

}

#endif