#ifndef __VANILA_CALL_FRAME_HH__
#define __VANILA_CALL_FRAME_HH__

#include "vanila/opcode.h"
#include "vanila/value.h"
#include "vanila/object.h"
#include <cstdint>

namespace vanila
{
struct CallFrame
{
    ObjectClosure* closure;
    OpCode* ip;
    Value* slots;

    CallFrame(ObjectClosure* c, OpCode* i, Value* s) :
        closure{c}, ip{i}, slots{s} {}
};

}

#endif