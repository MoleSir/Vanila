#ifndef __VANILA_DISASSEMBLER_HH__
#define __VANILA_DISASSEMBLER_HH__

#include "vanila/chunk.h"

namespace vanila
{
//! \brief the Dissembler class, use as a static class
class Disassembler
{
public:
    //! \brief disaemble the all bytecode in chunk
    static void disassembleChunk(const Chunk& chunk, const char* name) noexcept;
    
    //! \brief disassemble a bytecode at offset in the chunk
    static size_t disassembleInstruction(const Chunk& chunk, size_t offset) noexcept;
    
private:
    using Self = Disassembler;

    //! \brief disassemble a simple instruction
    static size_t simpleInstruction(const char* name, size_t offset) noexcept;
    
    //! \brief disassemble a constant instruction
    static size_t constantInstruction(const char* name, const Chunk& chunk, int offset) noexcept;
    
    //! \brief disassemble a byte instruction
    static size_t byteInstruction(const char* name, const Chunk& chunk, int offset) noexcept;

    //! \brief short instruction
    static size_t shortInstruction(const char* name, const Chunk& chunk, int offset) noexcept;

    //! \brief jump instruction
    static size_t jumpInstruction(const char* name, int sign, const Chunk& chunk, int offset) noexcept;

    // \brief invoke instruction
    static size_t invokeInstruction(const char* name, const Chunk& chunk, int offset) noexcept;
};

}

#endif