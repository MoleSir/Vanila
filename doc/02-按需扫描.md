# 按需扫描

## 扫描

有了一个简单的虚拟机框架，解释器的实现思路大致成型。首先需要输入源代码， 对源代码进行解析得到一串字节码与常量数组。之后送入到虚拟机中执行。

<img src="pics/2-按需扫描.assets/image-20230410110559543.png" alt="image-20230410110559543" style="zoom:50%;" />

现在要解决的就是编译这一步。这一步有两个部分：扫描器与编译器。扫码签的作用是将输入的字符串解析为一个一个的 Token（专有名词）。比如说 "if"、"while" 等关键字，"+"、"-" 等运算符，还有用户定义的变量名称。经过这一步解析器处理的不再是一个个字符，而是一个个按顺序排列的 Token。

例如这一行代码

````python
print 1 + 2;
````

经过扫描应该得到：

````c
   1 31 'print'
   | 21 '1'
   |  7 '+'
   | 21 '2'
   |  8 ';'
   2 39 ''
````

这些个 Token。



## Token

定义一个结构体表示 Token：

````c
enum class TokenType
{
    // Single-character tokens. 
    LEFT_PAREN, RIGHT_PAREN,
    LEFT_BRACE, RIGHT_BRACE,
    COMMA, DOT, MINUS, PLUS,
    SEMICOLON, SLASH, STAR,

    // One or two character tokens. 
    BANG, BANG_EQUAL,
    EQUAL, EQUAL_EQUAL,
    GREATER, GREATER_EQUAL,
    LESS, LESS_EQUAL,
    
    // Literals. 
    IDENTIFIER, STRING, NUMBER,
    
    // Keywords.
    AND, CLASS, ELSE, FALSE,
    FOR, FUN, IF, NIL, OR,
    PRINT, RETURN, SUPER, THIS,
    TRUE, VAR, WHILE,

    ERROR, ENDOFFILE
};

struct Token
{
    TokenType type;
    const char* start;
    uint32_t length;
    uint32_t line;
};
````

包含类型、字符内容与所在文件行数等信息。



## Sanner

扫描器的实现为一个 Scanner 类，包含两个字段：`start` 与 `current`，分别表示当前正常处理的 Token 的其实字符与当前遍历到的字符：

<img src="pics/2-按需扫描.assets/image-20230410110940086.png" alt="image-20230410110940086" style="zoom:50%;" />

初始化时，两个字段都指向源码的起始位置，之后向后移动。

扫描一个 Token 的实现并不复杂，对那些只有一个字符的 Token，就可以直接使用 `switch` 判断：

````c++
case '(': return this->makeToken(TokenType::LEFT_PAREN);
case ')': return this->makeToken(TokenType::RIGHT_PAREN);
case '{': return this->makeToken(TokenType::LEFT_BRACE);
case '}': return this->makeToken(TokenType::RIGHT_BRACE);
case ';': return this->makeToken(TokenType::SEMICOLON);
case ',': return this->makeToken(TokenType::COMMA);
case '.': return this->makeToken(TokenType::DOT);
case '-': return this->makeToken(TokenType::MINUS);
case '+': return this->makeToken(TokenType::PLUS);
case '/': return this->makeToken(TokenType::SLASH);
case '*': return this->makeToken(TokenType::STAR);
````

而有些 Token 稍微复杂，比如 `=` 与 `==`，不可以检测到 `=` 就确定了这个 Token，而需要再检测下一字符，同理对 `>`、`<` 等，所以实现为：

````c++
case '!':
    return this->makeToken( this->match('=') ? TokenType::BANG_EQUAL : TokenType::BANG );
case '=':
    return this->makeToken( this->match('=') ? TokenType::EQUAL_EQUAL : TokenType::EQUAL );
case '<':
    return this->makeToken( this->match('=') ? TokenType::LESS_EQUAL : TokenType::LESS );
case '>':
    return this->makeToken( this->match('=') ? TokenType::GREATER_EQUAL : TokenType::GREATER );   
````

除了这些还有字符串与数字类型，这两个类型的 Token 是不确定长度的。比如对字符串，当发现了 '"' 字符就说明发现了一个字符串，那么就应该不断向后遍历，直到发现了下一个 '"' 说明这个字符串结束了：

````c
Token Scanner::scanString()
{
    // jump all character include two '"'
    while (this->peek() != '"' && !this->isAtEnd())
    {
        if (this->peek() == '\n')
            this->_line++;
        this->advance();
    }

    if (this->isAtEnd())
        return this->errorToken("Unterminated string.");
    
    // jump '"'
    this->advance();
    return this->makeToken(TokenType::STRING);
}
````

对数字比较麻烦一些，按理说就只要发现其实为数字，并且一直遍历到没有数字即可，但问题是小数点的存在，需要多进行一次判断：

````c
Token Scanner::scanNumber()
{
    // jump all digit
    while (utils::isDigit(this->peek()))
        this->advance();

    // is a '.'?
    if (this->peek() == '.' && utils::isDigit(this->peekNext()))
    {
        // is so, jump the '.'
        this->advance();

        // try to consume the digit after '.'
        while (utils::isDigit(this->peek()))
            this->advance();
    }

    return this->makeToken(TokenType::NUMBER);
}
````

最后就是对语言保留字与用户自定义变量的判断。可以这样，一旦发现一个 Token 以字母或者下划线开头，就可能是保留字或者自定义的变量，首先变量这个 Token 获取其全部字符，定义一张字典，包含关键字 Token 类型与其字符串的映射，就可以用这个表判断一个字符串是否为关键字，如果不是就认为是一个自定义变量。

这个方法可以，但低效，因为做了很多无效的判断，更好的办法是生成一字典树，使用状态机判断。比如对 `and` 关键字，只要开头为 `a`，接下来就判断下一个是不是 `n`，如果不是那判断就结束了，这肯定不是一个保留字，如果还是就需要判断 `d`，以此类推。

那么对那些只有一种字符开头的关键字，可以这样判断：

````cpp
    case 'a': return this->checkKeyword(1, 2, "nd", TokenType::AND);
    case 'c': return this->checkKeyword(1, 4, "lass", TokenType::CLASS);
    case 'e': return this->checkKeyword(1, 3, "lse", TokenType::ELSE);
    case 'i': return this->checkKeyword(1, 1, "f", TokenType::IF);
    case 'n': return this->checkKeyword(1, 2, "il", TokenType::NIL);
    case 'o': return this->checkKeyword(1, 1, "r", TokenType::OR);
    case 'p': return this->checkKeyword(1, 4, "rint", TokenType::PRINT);
    case 'r': return this->checkKeyword(1, 5, "eturn", TokenType::RETURN);
    case 's': return this->checkKeyword(1, 4, "uper", TokenType::SUPER);
    case 'v': return this->checkKeyword(1, 2, "ar", TokenType::VAR);
    case 'w': return this->checkKeyword(1, 4, "hile", TokenType::WHILE);
````

而如果某个关键字开头一样，比如 `false`、`for`，那么就需要多次判断了：

````c
    case 'f':
    {
        if (this->_current - this->_start > 1)
        {
            switch (this->_start[1])
            {
            case 'a': return this->checkKeyword(2, 3, "lse", TokenType::FALSE);
            case 'o': return this->checkKeyword(2, 1, "r", TokenType::FOR);
            case 'u': return this->checkKeyword(2, 1, "n", TokenType::FUN);
            }
        }
        break;
    }
    case 't':
    {
        if (this->_current - this->_start > 1)
        {
            switch (this->_start[1])
            {
            case 'h': return this->checkKeyword(2, 2, "is", TokenType::THIS);
            case 'r': return this->checkKeyword(2, 2, "ue", TokenType::TRUE);
            }
        }
        break;
    }
````

