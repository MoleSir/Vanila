# 7 全局变量

在虚拟机对象中使用一张 `std::map<std::map<ObjectString*, Value>> _globals` 保存全局变量名称与其值的键值对。

定义全局变量就是在 `_globals` 中插入一项，修改与取值就是在 `_globals` 中查找。



## 7.1 定义全局变量

### 7.1.1 编译过程

当在全局作用域中遇到 `var` 的 Token，那么就是定义一个全局变量。

在编译过程中，首先将这个变量的名称封装为一个 `ObjectString` 对象，并且添加到 `chunk` 对象中的常量表中，并且返回其索引：

````cpp
uint8_t makeIdentifierConstant(Token* name)
{ 
	return this->makeConstant(Value(reinterpret_cast<Object*>(ObjectString::allocate(name->start, name->length)))); 
}
````

接下来，判断变量名后是否有 `=`，如果有就表示需要给这个全局变量赋值，如果没有就默认赋值为 `NIL`：

````cpp
if (this->matchTokenType(TokenType::EQUAL))
    this->compileExpression();
else
    this->emitBytecode(OpCode::NIL);
````

最后将定义全局变量字节码与全局命令名称索引插入字节码数组。

综上，编译一句全局变量定义语句，会给字节码数组增加至少三条命令：

- `OpCode::CONSTANT`：全局变量初始值；
- `OpCode::DEFINE_GLOBAL`：定义全局变量指令；
- `index`：全局命令名称字符串位于常量表的索引。

> 当前，如果全局变量的初始值需要计算，那么就不止三条，需要在一开始增加计算初始值的字节码。

### 7.1.2 执行过程

执行 `OpCode::DEFINE_GLOBAL` 的过程很简单，当执行到 `OpCode::DEFINE_GLOBAL` 时，全局变量的初始值已经被计算得到，并且放置在栈顶，而后一条字节码是全局命令名称字符串位于常量表的索引。

执行命令的过程是：

1. 读取字符串索引，查找到全局命令名称；
2. 向 `_globals` 添加一个键值对 {全局变量名称、初始值}。
3. 最后弹出栈顶的初值。

````cpp
ObjectString* name = reinterpret_cast<ObjectString*>(this->readConstant().object());
this->_globals[name] = this->peek(0);
this->pop();
````



## 7.2 使用/设置全局变量

### 7.2.1 编译过程

当在全局范围遇到了一个 `identifier` 类型的 Token，那么这个就是一个使用或者设置全局变量。判断的依据就是下一个 Token 是否为 `EQUAL`。

但不管是设置还是查找，都需要把这个 `identifier` 类型的 Token 封装为 `ObjectString*` 插入常量表，并且得到索引 `arg`。

`````cpp
arg = this->makeIdentifierConstant(&name);
`````

如果是使用全局变量，编译过程很简单，直接发送 `OpCode::GET_GLOBAL` 字节码与 `arg` 即可：

````cpp
this->emitBytecodes(getOp, static_cast<OpCode>(arg)); 
````

如果是设置全局变量，需要先编译等号后面的值，再发送 `OpCode::SET_GLOBAL` 字节码与 `arg` ：

````cpp
this->compileExpression();
this->emitBytecodes(setOp, static_cast<OpCode>(arg));
````

### 7.2.2 执行过程

当执行到字节码 `OpCode::GET_GLOBAL` 或 `OpCode::SET_GLOBAL` 时，下一个字节码表示全局变量名称字符串在常量表中的索引。首先获得这个变量名称：

```cpp
ObjectString* name = reinterpret_cast<ObjectString*>(this->readConstant().object());
```

并且需要进行判断，这个 `ObjectString` 对象是否存在于 `_globals` 中，即判断变量是否被定义：

````c++
auto iter = this->_globals.find(name);
if (iter == this->_globals.end())
	throw RuntimeError(this->currentLine(), "Undefined variable '" + name->str() + "'.");
````

最后如果判断到了这个变量确实存在，那么根据是使用变量还是设置变量：

- 使用变量：很简单，现在已经找到了变量名对应的键值，即为变量值，要获取这个变量就把这个 `Value` 插入到栈顶即可：

  ````cpp
  this->push(iter->second);
  ````

- 如果是设置变量，当执行到这里，其新的值已经被求值放在栈顶了，要修改只要把栈顶赋值过去即可：

  ````c++
  iter->second = this->peek(0);
  ````

  
