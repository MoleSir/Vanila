s = 

# 伪目标
.PHYON: run, clean, launch, debug, release, git, test

debug:
	@python ./scripts/compile.py debug

# 运行exe文件
run:
	@python ./scripts/run.py debug $(s)

clean:
	@python ./scripts/clean.py debug

release:
	@python ./scripts/compile.py release

test:
	@python ./scripts/run.py debug './vanila/test.va'

comment = 
git:
	@python ./scripts/git.py $(comment)