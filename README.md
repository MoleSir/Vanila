# Vanila

基于 C++ 语言实现的 Vanila 语言解释器。



## 当前任务

类的静态成员

全局第一层作用域存在 bug



## 实现功能

### 基本语法

- [x] 字节码
- [x] 虚拟机
- [x] 字符串类型
- [x] 函数对象与调用
- [x] 闭包
- [x] 垃圾回收
- [x] 类
- [ ] `import` 关键字

### 内建功能

- [x] 列表
- [x] 字典
- [x] 集合
- [ ] 读写文件



## 参考

- [munificent/craftinginterpreters: Repository for the book "Crafting Interpreters" (github.com)](https://github.com/munificent/craftinginterpreters)