#include "vanila/native.h"
#include "vanila/value.h"
#include "vanila/object.h"
#include "vanila/virtualmachine.h"
#include <chrono>

namespace vanila
{
static auto beginTick = std::chrono::steady_clock::now();

//! \brief get current tick(s)
Value Native::System::clock(uint32_t argCount, Value* args)
{
    Native::assertArgumentCount(argCount, 0, "clock");

    auto current = std::chrono::steady_clock::now();
    float tick = std::chrono::duration<float>(current - beginTick).count();
    return Value(tick);
}

Value Native::System::exit(uint32_t argCount, Value* args)
{
    return Value(ValueType::NIL);
}

Value Native::System::showStack(uint32_t argCount, Value* args)
{
    Native::assertArgumentCount(argCount, 1, "show_stack");
    Native::vm->setShowStack(args[0].boolean());
    return Value(ValueType::NIL);
}

Value Native::System::showBytecodes(uint32_t argCount, Value* args)
{
    Native::assertArgumentCount(argCount, 1, "show_bytecodes");
    Native::vm->setShowBytecodes(args[0].boolean());
    return Value(ValueType::NIL);
}

Value Native::System::print(uint32_t argCount, Value* args)
{   
    for (uint32_t i = 0; i < argCount; i++)
    {
        args[i].print();
        std::cout << ' ';
    } 
    std::cout << '\n';

    return Value(ValueType::NIL);
}

Value Native::System::hash(uint32_t argCount, Value* args)
{    
    Native::assertArgumentCount(argCount, 1, "hash");
    return Value(args->hash());
}

}