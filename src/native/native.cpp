#include "vanila/native.h"
#include "vanila/virtualmachine.h"
#include "vanila/allocator.h"
#include "utils/format.h"

namespace vanila
{
VirtualMachine* Native::vm = utils::Singleton<VirtualMachine>::instance();
Allocator* Native::allocator = utils::Singleton<Allocator>::instance();

//! \brief check native function's arguments count
void Native::assertArgumentCount(uint32_t argCount, uint32_t targetCount, const char* name)
{
    if (argCount != targetCount)
        Native::vm->runtimeError(utils::format("The native <fn %s> need %d arguments but get %d.", name, targetCount, argCount)); 
}

}