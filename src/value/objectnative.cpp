#include "vanila/baseobject.h"
#include <iostream>

namespace vanila
{
//! \brief Construct a new Object Native:: Object Native object
ObjectNative::ObjectNative(NativeFunction function) noexcept :
    Object{ObjectType::NATIVE}, _function{function}
{}

//! \brief print object list
void ObjectNative::print(bool callStr) const
{
    static_cast<void>(callStr);
    std::cout << "<native fn>";
}

//! \brief execute the native c++ function
//! \param[in] argCount arguments count
//! \param[in] args argsument array pointer
//! \return Value
Value ObjectNative::execute(uint32_t argCount, Value* args) const
{
    return this->_function(argCount, args);
}

}