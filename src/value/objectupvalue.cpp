#include "vanila/baseobject.h"
#include <iostream>

namespace vanila
{
//! \brief Construct a new Object Upvalue:: Object Upvalue object
ObjectUpvalue::ObjectUpvalue(Value* slot) noexcept : 
    Object{ObjectType::UPVALUE}, 
    _location{slot}, 
    _closed{}, 
    _next{nullptr}
{}

//! \brief print object upvalue
//! \param[in] callStr wheather call '__str__' method to print
void ObjectUpvalue::print(bool callStr) const
{
    static_cast<void>(callStr);
    std::cout << "upvalue";
}

}