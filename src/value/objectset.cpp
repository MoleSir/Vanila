#include "vanila/dsobject.h"
#include <set>

namespace vanila
{

ObjectSet::ObjectSet() : 
    Object{ObjectType::SET}, _set{}
{
}

ObjectSet::ObjectSet(std::set<Value> set):
    Object{ObjectType::SET}, _set{std::move(set)}
{
}

//! \brief print object list
//! \param[in] callStr wheather call '__str__' method to print
void ObjectSet::print(bool callStr) const
{
    static_cast<void>(callStr);
    std::cout << '{';
    
    bool first = true;
    for (const Value& value : this->_set)
    {
        if (!first)
            std::cout << ", ";
        value.print();
        first = false;
    }

    std::cout << '}';
}

//! \brief wheather this == that
bool ObjectSet::equal(const Object* that) const
{
    const ObjectSet* other = that->asSet();
    if (this->_set.size() != other->_set.size())
        return false;
    
    auto iter1 = this->_set.cbegin();
    auto iter2 = other->_set.cbegin();
    for (size_t i = 0; i < this->_set.size(); ++i)
    {
        if ( !iter1->equal(*iter2))
            return false;
        iter1++;
        iter2++;
    }

    return true;
}

//! \brief weather the set contains the value
bool ObjectSet::contain(const Value& value)
{
    auto iter = this->_set.find(value);
    if (iter == this->_set.end())
        return false;
    return true;
}

}