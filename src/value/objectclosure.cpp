#include "vanila/baseobject.h"

namespace vanila
{
//! \brief Construct a new Object Closure object
ObjectClosure::ObjectClosure(ObjectFunction* function) noexcept:
    Object{ObjectType::CLOSURE}, _function{function}, _upvalues(function->upvalueCount(), nullptr)
{
}

//! \brief print object closure
//! \param[in] callStr wheather call '__str__' method to print
void ObjectClosure::print(bool callStr) const
{
    this->_function->print(callStr);
}

}