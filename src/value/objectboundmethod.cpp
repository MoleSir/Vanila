#include "vanila/baseobject.h"

namespace vanila
{
//! \brief Construct a new Object Bound Method:: Object Bound Method object
ObjectBoundMethod::ObjectBoundMethod(const Value& receiver, Object* method) noexcept:
    Object{ObjectType::BOUND_METHOD},
    _recevier{receiver}, 
    _method{method}
{}

//! \brief print object bound method
//! \param[in] callStr wheather call '__str__' method to print
void ObjectBoundMethod::print(bool callStr) const
{
    this->_method->print(callStr);
}


}