#include "vanila/baseobject.h"
#include <iostream>

namespace vanila
{
//! \brief Construct a new Object Function:: Object Function object
ObjectFunction::ObjectFunction() : 
    Object(ObjectType::FUNCTION), _arity{0}, _upvalueCount{0}, _chunk{}, _name{nullptr}
{
    
}

//! \brief print object function
//! \param[in] callStr wheather call '__str__' method to print
void ObjectFunction::print(bool callStr) const
{
    static_cast<void>(callStr);
    if (this->_name == nullptr)
    {
        std::cout << "<script>";
        return;
    }
    std::cout << "<fn " << this->_name->str() << '>';
}

}