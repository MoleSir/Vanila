#include "vanila/baseobject.h"
#include "vanila/allocator.h"
#include "utils/hash.h"
#include <iostream>
#include <string>

namespace vanila
{
static Allocator* allocator = utils::Singleton<Allocator>::instance();

//! \brief Construct a new Object String:: Object String object
//! \param[in] chars the start of the c-stirng
//! \param[in] length the length of the c-stirng
ObjectString::ObjectString(const char* chars, uint32_t length): 
    Object{ObjectType::STRING}, _str(chars, length) 
{

}

//! \brief Construct a new Object String:: Object String object
//! \param[in] str std::string object
ObjectString::ObjectString(std::string str):
    Object{ObjectType::STRING}, _str{std::move(str)}
{
}

//! \brief print object string
//! \param[in] callStr wheather call '__str__' method to print
void ObjectString::print(bool callStr) const
{
    static_cast<void>(callStr);
    std::cout << this->_str;
}

//! \brief wheather this < that
bool ObjectString::less(const Object* that) const
{
    return this->_str < that->asString()->_str;
}

//! \brief wheather this > that
bool ObjectString::greater(const Object* that) const
{
    return this->_str > that->asString()->_str;
}

//! \brief add two object
Value ObjectString::add(const Object* that) const
{
    if (!that->isString())
        return Object::add(that);
    return Value{
        allocator->allocateString(this->str() + that->asString()->str())
    };
}

}