#include "vanila/vanila.h"

int main(int argc, const char* argv[])
{
    vanila::Vanila::init();

    if (argc == 1)
        vanila::Vanila::interactive();
    else if (argc == 2)
        vanila::Vanila::runFile(argv[1]);
    else
        exit(64);

    vanila::Vanila::release();

    return 0;
}
