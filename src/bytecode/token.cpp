#include "vanila/token.h"
#include <cstring>

namespace vanila
{
Token::Token(const char* text) noexcept:
    Token(TokenType::LEFT_PAREN, text, strlen(text), 0)
{

}


//! \brief check wheather two tokens name is equal
bool Token::identifiersEqual(const Token& a, const Token& b) noexcept
{
    if (a.length != b.length)
        return false;
    return memcmp(a.start, b.start, a.length) == 0;
}
}