#include "vanila/value.h"
#include "vanila/chunk.h"

namespace vanila
{
Chunk::Chunk() : _codes{}, _constants{}, _lines{}
{}

Chunk::~Chunk() noexcept 
{}

//! \brief add a constant value to chunk's constants vector
//! \param[in] value the constant object to insert
//! \return size_t the index of value in constant vector
size_t Chunk::addConstant(const Value& value)
{
    this->_constants.push_back(value);
    return this->_constants.size() - 1;
}

//! \brief insert a byet to code's vector
//! \param[in] byte the byte value
//! \param[in] line the line of byte in file
void Chunk::writeByte(uint8_t byte, int line)
{
    this->writeBytecode(static_cast<OpCode>(byte), line);
}

//! \brief insert a opcode to code's vector
//! \param[in] opcode the operator bytecode
//! \param[in] line the line of opcode in file
void Chunk::writeBytecode(OpCode opcode, int line)
{ 
    this->_codes.push_back(opcode);
    this->_lines.push_back(line);
}

//! \brief write a CONSTANT byte code 
void Chunk::writeConstant(const Value& value, int line)
{
    uint8_t constantIndex = this->addConstant(value);
    this->writeBytecode(OpCode::CONSTANT, line);
    this->writeByte(constantIndex, line);
}

}