#include "utils/hash.h"

namespace utils
{
uint32_t fnv1aHash(const char* key, uint32_t length) noexcept
{
    uint32_t hash = 2166136261u;
    for (uint32_t i = 0; i < length; i++) 
    {
        hash ^= (uint8_t)key[i];
        hash *= 16777619;
    }
    return hash;
}
}