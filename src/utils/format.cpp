#include "utils/format.h"
#include <cstdarg>

namespace utils
{
//! \brief format the string to a std::string
//! \param[in] fmt format string
//! \param[in] ... arguments
//! \return std::string
std::string format(const char* fmt, ...)
{
    va_list args;
    va_start(args, fmt);
    int len = vsnprintf(nullptr, 0, fmt, args);

    std::string str(len + 1, 0);
    vsnprintf(&(str.front()), len + 1, fmt, args);
    va_end(args);

    return str;
}
}