#include "vanila/error.h"
#include <string>
#include <iostream>

namespace vanila
{
std::string RuntimeError::what() const
{
    return "Runtime error!\n";
}
}